﻿using System.Collections.Generic;
using UnityEngine;

public class CubePiece : MonoBehaviour
{
    public bool split = false;
    public CubePieceParent parent;
    public int X
    {
        get;
        set;
    }
    public int Y
    {
        get;
        set;
    }
    public int Z
    {
        get;
        set;
    }
    public Direction[] possibleDirections
    {
        get;
        private set;
    }

    public void init(int x, int y, int z, int length, int width, int height)
    {
        X = x;
        Y = y;
        Z = z;

        List<Direction> dirs = new List<Direction>();
        if (x < length - 1)
        {
            dirs.Add(Direction.right);
        }
        if (x > 0)
        {
            dirs.Add(Direction.left);
        }
        if (y < width - 1)
        {
            dirs.Add(Direction.forward);
        }
        if (y > 0)
        {
            dirs.Add(Direction.back);
        }
        if (z < height - 1)
        {
            dirs.Add(Direction.up);
        }
        if (z > 0)
        {
            dirs.Add(Direction.down);
        }
        possibleDirections = dirs.ToArray();
    }
}