﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum Direction
{
    up,
    down,
    forward,
    back,
    left,
    right
}

public class CubeManager : MonoBehaviour
{
    public InputController IC;
    public GameObject arrowPrefab;
    public GameObject gridPrefab;
    public int pieces = 4;
    [Range(2, 9)]
    public int x_length = 3;
    [Range(2, 9)]
    public int z_width = 3;
    [Range(2, 9)]
    public int y_height = 3;
    public GameObject cubePrefab;
    public GameObject placerPrefab;
    Queue<CubePiece> cubeQ = new Queue<CubePiece>();
    CubePieceParent[] blocks;
    public GameObject[] arrows;
    CubePieceParent selectedPiece;
    CubePiece[,,] board;
    public GameObject winPanel;
    public InputField widthField;
    public InputField lengthField;
    public InputField heightField;
    public InputField piecesField;
    public Material outline;
    Vector3 CenterPoint;

    public CubePieceParent SelectedPiece
    {
        get
        {
            return selectedPiece;
        }
        set
        {
            if (selectedPiece != null && selectedPiece != value)
            {
                foreach (var block in selectedPiece.clone.children)
                {
                    var mats = block.GetComponent<Renderer>().materials.ToList();
                    if (mats.Count == 2)
                    {
                        mats.RemoveAt(mats.Count - 1);
                        block.GetComponent<Renderer>().materials = mats.ToArray();
                    }
                }
            }
            else
            {
                selectedPiece = value;
                if (selectedPiece != null)
                {
                    foreach (var block in selectedPiece.clone.children)
                    {
                        var mats = block.GetComponent<Renderer>().materials.ToList();
                        if (mats.Count == 1)
                        {
                            mats.Add(outline);
                            block.GetComponent<Renderer>().materials = mats.ToArray();
                        }
                    }
                }
            }
            selectedPiece = value;
            if (selectedPiece != null)
            {
                IC.CameraLookAt = selectedPiece.Center;
                foreach (var arrow in arrows)
                {
                    arrow.gameObject.SetActive(true);
                    arrow.transform.parent = selectedPiece.clone.transform;
                }
                Vector3 max = selectedPiece.Maximum;
                Vector3 min = selectedPiece.Minimum;
                arrows[0].transform.position = new Vector3((max.x + min.x) / 2, max.y + 0.5f, (max.z + min.z) / 2);
                arrows[1].transform.position = new Vector3((max.x + min.x) / 2, min.y-+ 0.5f, (max.z + min.z) / 2);
                arrows[2].transform.position = new Vector3(max.x + 0.5f, (max.y + min.y) / 2, (max.z + min.z) / 2);
                arrows[3].transform.position = new Vector3(min.x - 0.5f, (max.y + min.y) / 2, (max.z + min.z) / 2);
                arrows[4].transform.position = new Vector3((max.x + min.x) / 2, (max.y + min.y) / 2, max.z + 0.5f);
                arrows[5].transform.position = new Vector3((max.x + min.x) / 2, (max.y + min.y) / 2, min.z-+ 0.5f);
            }
            else
            {
                IC.CameraLookAt = CenterPoint;
                foreach(var arrow in arrows)
                {
                    arrow.gameObject.SetActive(false);
                }
            }
        }
    }

    public float Size
    {
        get;
        private set;
    }

    public void ResetScene()
    {
        SceneManager.LoadScene(0);
    }

    public void CheckSizeField()
    {
        bool changed = false;
        int X;
        int Z;
        int Y;
        if (int.TryParse(widthField.text, out Z))
        {
            changed = true;
            if (Z < 2)
            {
                Z = 2;
                widthField.text = Z.ToString();
            }
        }
        else
        {
            Z = 3;
            widthField.text = "";
        }
        if (int.TryParse(lengthField.text, out X))
        {
            changed = true;
            if (X < 2)
            {
                X = 2;
                lengthField.text = X.ToString();
            }
        }
        else
        {
            X = 3;
            lengthField.text = "";
        }
        if (int.TryParse(heightField.text, out Y))
        {
            changed = true;
            if (Y < 2)
            {
                Y = 2;
                heightField.text = Y.ToString();
            }
        }
        else
        {
            Y = 3;
            heightField.text = "";
        }
        //Size = Mathf.Max(x_length, y_height, z_width);
        Size = (x_length + y_height + z_width) / 3f;
        x_length = X;
        y_height = Y;
        z_width = Z;
        if (changed)
        {
            CheckPiecesField();
        }
    }

    public void CheckPiecesField()
    {
        int sz;
        if (int.TryParse(piecesField.text, out sz))
        {
            if (sz < 2)
            {
                sz = 2;
            }
            else if (sz > x_length * z_width * y_height)
            {

                sz = x_length * z_width * y_height;
            }
            piecesField.text = sz.ToString();
        }
        else
        {
            sz = 5;
            piecesField.text = null;
        }
        pieces = sz;
    }

    public void Begin()
    {
        CheckSizeField();
        Camera.main.transform.position *= Size;
        this.CenterPoint = new Vector3(x_length * 1.5f - 0.5f, y_height * 1.5f - 0.5f, z_width * 1.5f - 0.5f);
        IC.CameraCenter.transform.position = this.CenterPoint;
        board = new CubePiece[x_length * 3, y_height * 3, z_width * 3];
        float scale = Size / 4f + 0.75f;
        foreach (var arrow in arrows)
        {
            arrow.transform.localScale = Vector3.one * scale;
        }

        //Vector3 CenterPoint = new Vector3(x_length + (x_length / 2f) - 0.5f, y_height + (y_height / 2f) - 0.5f, z_width + (z_width / 2f) - 0.5f);
        GameObject obj = Instantiate(gridPrefab, Vector3.forward * z_width * 1.5f + CenterPoint, Quaternion.Euler(0, 0, 0));
        obj.GetComponent<Renderer>().material.mainTextureScale = new Vector2(x_length * 3, y_height * 3);
        obj.transform.localScale = new Vector3(x_length * 3, y_height * 3, 0);

        obj = Instantiate(gridPrefab, Vector3.back * z_width * 1.5f + CenterPoint, Quaternion.Euler(0, 180, 0));
        obj.GetComponent<Renderer>().material.mainTextureScale = new Vector2(x_length * 3, y_height * 3);
        obj.transform.localScale = new Vector3(x_length * 3, y_height * 3, 0);

        obj = Instantiate(gridPrefab, Vector3.right * x_length * 1.5f + CenterPoint, Quaternion.Euler(0, 90, 0));
        obj.GetComponent<Renderer>().material.mainTextureScale = new Vector2(z_width * 3, y_height * 3);
        obj.transform.localScale = new Vector3(z_width * 3, y_height * 3, 0);

        obj = Instantiate(gridPrefab, Vector3.left * x_length * 1.5f + CenterPoint, Quaternion.Euler(0, 270, 0));
        obj.GetComponent<Renderer>().material.mainTextureScale = new Vector2(z_width * 3, y_height * 3);
        obj.transform.localScale = new Vector3(z_width * 3, y_height * 3, 0);

        obj = Instantiate(gridPrefab, Vector3.up * y_height * 1.5f + CenterPoint, Quaternion.Euler(270, 0, 0));
        obj.GetComponent<Renderer>().material.mainTextureScale = new Vector2(x_length * 3, z_width * 3);
        obj.transform.localScale = new Vector3(x_length * 3, z_width * 3, 0);

        obj = Instantiate(gridPrefab, Vector3.down * y_height * 1.5f + CenterPoint, Quaternion.Euler(90, 0, 0));
        obj.GetComponent<Renderer>().material.mainTextureScale = new Vector2(x_length * 3, z_width * 3);
        obj.transform.localScale = new Vector3(x_length * 3, z_width * 3, 0);

        placerPrefab = Instantiate(placerPrefab, CenterPoint, Quaternion.identity);
        placerPrefab.transform.localScale = new Vector3(x_length, y_height, z_width);

        blocks = new CubePieceParent[pieces];
        CreateCube();
        foreach(var block in blocks)
        {
            block.InitBounds();
            Color col = Random.ColorHSV();
            /*block.gameObject.AddComponent<MeshRenderer>();
            block.gameObject.AddComponent<MeshFilter>();
            block.GetComponent<Renderer>().material = block.children[0].GetComponent<Renderer>().material;
            block.GetComponent<Renderer>().material.color = new Color(col.r, col.g, col.b, 0.75f);
            CombineInstance[] combine = new CombineInstance[2];
            int offset1 = 0;
            int offset2;*/
            foreach (var cube in block.children)
            {
                cube.GetComponent<Renderer>().material.color = new Color(col.r, col.g, col.b, 0.75f);
                /*MeshFilter mf = cube.GetComponent<MeshFilter>();
                combine[0].mesh = block.GetComponent<MeshFilter>().sharedMesh;
                combine[0].transform = block.transform.localToWorldMatrix;
                combine[1].mesh = mf.sharedMesh;
                combine[1].transform = mf.transform.localToWorldMatrix;
                offset2 = block.GetComponent<MeshFilter>().mesh.triangles.Length;
                Mesh newMesh = new Mesh();
                newMesh.CombineMeshes(combine, true);
                DeleteFaces(newMesh, offset1, offset2);
                offset1 = offset2;
                block.GetComponent<MeshFilter>().mesh = newMesh;
                DestroyImmediate(mf);
                DestroyImmediate(cube.GetComponent<Renderer>());*/
            }
            //
            //break;
        }
        UnmakeCube();
        foreach (var block in blocks)
        {
            block.clone = Instantiate(block).GetComponent<CubePieceParent>();
            Color col = block.children[0].GetComponent<Renderer>().material.color;
            //block.clone.GetComponent<Renderer>().material.color = new Color(col.r, col.g, col.b, 1.0f);
            foreach (var cube in block.clone.children)
            {
                cube.GetComponent<Renderer>().material.color = new Color(col.r, col.g, col.b, 1f);
                Destroy(cube.GetComponent<Collider>());
            }
        }
        IC.Begin();
    }

    void DeleteFaces(Mesh mesh, int offset1, int offset2)
    {
        Debug.Log("triangles: " + mesh.triangles.Length);
        Debug.Log("offset1: " + offset1 + ", offset2: " + offset2);
        var verts = mesh.vertices;
        List<int> deleteVerts = new List<int>();
        List<int> deleteVerts2 = new List<int>();
        List<int> deleteVerts3 = new List<int>();
        var tris = mesh.triangles;
        for (int tri1 = offset1; tri1 < offset2; tri1 += 3)
        {
            for (int tri2 = offset2; tri2 < tris.Length; tri2 += 3)
            {
                if (deleteVerts2.Contains(tri2))
                {
                    continue;
                }
                var verts1 = new Vector3[] { verts[tris[tri1]], verts[tris[tri1 + 1]], verts[tris[tri1 + 2]] };
                var verts2 = new Vector3[] { verts[tris[tri2]], verts[tris[tri2 + 1]], verts[tris[tri2 + 2]] };
                
                if (Facing(verts1, verts2))
                {
                    Debug.Log("tri1: " + tri1 + ", tri2: " + tri2 + "\nremoving triangles: " + verts1[0] + ", " + verts1[1] + ", " + verts1[2] + ", " + verts2[0] + ", " + verts2[1] + ", " + verts2[2]);
                    //Debug.Log("removing triangles");
                    deleteVerts2.Add(tri2);
                    deleteVerts3.Add(tri1);
                    deleteVerts.Add(tris[tri1]);
                    deleteVerts.Add(tris[tri1] + 1);
                    deleteVerts.Add(tris[tri1] + 2);
                    deleteVerts.Add(tris[tri2]);
                    deleteVerts.Add(tris[tri2] + 1);
                    deleteVerts.Add(tris[tri2] + 2);
                    break;
                }
            }
        }
        List<int> newTris = new List<int>(tris.Length - deleteVerts2.Count - deleteVerts3.Count);
        for (int i = 0; i < tris.Length; i++)
        {
            if (deleteVerts2.Contains(i) || deleteVerts3.Contains(i))
            {
                i += 2;
                continue;
            }
            newTris.Add(tris[i]);
        }
        Debug.Log("old triangles: " + tris.Length + ", new triangles: " + newTris.Count);
        mesh.triangles = newTris.ToArray();
    }
    enum Dimension
    {
        X,
        Y,
        Z,
        None
    }
    bool Facing(Vector3[] verts1, Vector3[] verts2)
    {
        foreach(var vert in verts1)
        {
            foreach(var point in verts2)
            {
                if (Vector3.Distance(vert, point) > 2f)
                {
                    return false;
                }
            }
        }
        float value;
        return IsFlat(verts2, IsFlat(verts1, out value), value);
    }
    Dimension IsFlat(Vector3[] verts, out float value)
    {
        Vector3 val = verts[0];
        bool isX = true;
        bool isY = true;
        bool isZ = true;
        foreach (var vert in verts)
        {
            if (vert.x != val.x)
            {
                isX = false;
            }
            if (vert.y != val.y)
            {
                isY = false;
            }
            if (vert.z != val.z)
            {
                isZ = false;
            }
        }
        if (isX)
        {
            value = val.x;
            return Dimension.X;
        }
        if (isY)
        {
            value = val.y;
            return Dimension.Y;
        }
        if (isZ)
        {
            value = val.z;
            return Dimension.Z;
        }
        value = 0;
        return Dimension.None;
    }
    bool IsFlat(Vector3[] verts, Dimension dim, float value)
    {
        if (dim == Dimension.None)
        {
            return false;
        }
        foreach (var vert in verts)
        {
            switch (dim)
            {
                case Dimension.X:
                    {
                        if (vert.x != value)
                        {
                            return false;
                        }
                        break;
                    }
                case Dimension.Y:
                    {
                        if (vert.y != value)
                        {
                            return false;
                        }
                        break;
                    }
                case Dimension.Z:
                    {
                        if (vert.z != value)
                        {
                            return false;
                        }
                        break;
                    }
                default:
                    {
                        return false;
                    }
            }
        }
        return true;
    }

    void Shuffle<T>(IList<T> list, System.Random rng)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    void CreateCube()
    {
        //float shift = -Size; //(size / 2f) - 0.5f;
        List<CubePiece> cubeList = new List<CubePiece>();
        CubePiece[,,] cubes = new CubePiece[x_length, y_height, z_width];
        for (int x = 0; x < x_length; x++)
        {
            for (int y = 0; y < y_height; y++)
            {
                for (int z = 0; z < z_width; z++)
                {
                    CubePiece cp = Instantiate(cubePrefab, new Vector3(x + x_length, y + y_height, z + z_width), Quaternion.identity).AddComponent<CubePiece>();
                    cp.init(x, y, z, x_length, y_height, z_width);
                    cubes[x, y, z] = cp;
                    cubeList.Add(cp);
                }
            }
        }
        Shuffle(cubeList, new System.Random());
        foreach(var item in cubeList)
        {
            cubeQ.Enqueue(item);
        }
        SplitCube(cubes);
    }

    void SplitCube(CubePiece[,,] cubes)
    {
        for (int i = 0; i < pieces; i++)
        {
            blocks[i] = new GameObject("Block " + i).AddComponent<CubePieceParent>();
            CubePiece piece = cubeQ.Dequeue();
            blocks[i].children.Add(piece);
            piece.split = true;
            piece.parent = blocks[i];
            piece.transform.parent = piece.parent.transform;
        }

        while(cubeQ.Count > 0)
        {
            CubePiece piece = cubeQ.Dequeue();
            int len = -1;
            CubePiece other = null;
            for (int i = 0; i < piece.possibleDirections.Length; i++)
            {
                CubePiece item = null;
                Direction dir = piece.possibleDirections[i];
                switch (dir)
                {
                    case Direction.up:
                        {
                            item = cubes[piece.X, piece.Y, piece.Z + 1];
                            break;
                        }
                    case Direction.down:
                        {
                            item = cubes[piece.X, piece.Y, piece.Z - 1];
                            break;
                        }
                    case Direction.forward:
                        {
                            item = cubes[piece.X, piece.Y + 1, piece.Z];
                            break;
                        }
                    case Direction.back:
                        {
                            item = cubes[piece.X, piece.Y - 1, piece.Z];
                            break;
                        }
                    case Direction.left:
                        {
                            item = cubes[piece.X - 1, piece.Y, piece.Z];
                            break;
                        }
                    case Direction.right:
                        {
                            item = cubes[piece.X + 1, piece.Y, piece.Z];
                            break;
                        }
                }
                if (!item.split)
                {
                    continue;
                }
                if (len == -1 || item.parent.children.Count < len)
                {
                    len = item.parent.children.Count;
                    other = item;
                }
            }

            if (other == null)
            {
                cubeQ.Enqueue(piece);
                continue;
            }
            other.parent.children.Add(piece);
            piece.parent = other.parent;
            piece.transform.parent = piece.parent.transform;
            piece.split = true;
        }
    }

    void UnmakeCube()
    {
        foreach(var block in blocks)
        {
            foreach(var cube in block.children)
            {
                cube.X = (int) cube.transform.position.x;
                cube.Y = (int)cube.transform.position.y;
                cube.Z = (int)cube.transform.position.z;
            }
        }
        
        float x = 0;
        float y = 0;
        float z = 0;

        foreach (var block in blocks)
        {
            bool breakWhole = false;
            while (y < y_height * 3)
            {
                while (x < x_length * 3)
                {
                    while (z < z_width * 3)
                    {
                        if (!OverLaps(block, new Vector3(x, y, z)))
                        {
                            foreach (var cube in block.children)
                            {
                                board[cube.X, cube.Y, cube.Z] = null;
                            }
                            block.MoveMinimumTo(new Vector3(x, y, z));
                            foreach(var cube in block.children)
                            {
                                board[cube.X, cube.Y, cube.Z] = cube;
                            }
                            z++;
                            breakWhole = true;
                            break;
                        }
                        z++;
                    }
                    if (breakWhole)
                    {
                        break;
                    }
                    else
                    {
                        z = 0;
                    }
                    x++;
                }
                if (breakWhole)
                {
                    break;
                }
                else
                {
                    x = 0;
                }
                y++;
            }
            if (!breakWhole)
            {
                y = 0;
            }
        }
    }

    bool OverLaps(CubePieceParent settler, Vector3 settlePoint)
    {
        bool overlaps = false;
        foreach (var cube in settler.children)
        {
            board[cube.X, cube.Y, cube.Z] = null;
        }
        Vector3 dif = settler.Minimum - settlePoint;
        foreach(var cube in settler.children)
        {
            int x = cube.X - (int)dif.x;
            int y = cube.Y - (int)dif.y;
            int z = cube.Z - (int)dif.z;
            if (x >= x_length * 3 || y >= y_height * 3 || z >= z_width * 3 || x < 0 || y < 0 || z < 0 || board[x, y, z] != null)
            {
                overlaps = true;
            }
        }
        foreach (var cube in settler.children)
        {
            board[cube.X, cube.Y, cube.Z] = cube;
        }
        return overlaps;
    }

    public void TranslateSelectedPiece(Vector3 translation)
    {
        Vector3 target = selectedPiece.clone.Minimum + translation;
        selectedPiece.clone.MoveMinimumTo(target);
        target = selectedPiece.clone.Minimum;
        target = new Vector3(Mathf.Round(target.x), Mathf.Round(target.y), Mathf.Round(target.z));

        if (target != selectedPiece.Minimum && !OverLaps(selectedPiece, target))
        {
            foreach (var cube in selectedPiece.children)
            {
                board[cube.X, cube.Y, cube.Z] = null;
            }
            selectedPiece.MoveMinimumTo(target);
            foreach (var cube in selectedPiece.children)
            {
                board[cube.X, cube.Y, cube.Z] = cube;
            }
        }
    }

    public void CancelDrag()
    {
        if (selectedPiece != null)
        {
            selectedPiece.clone.MoveMinimumTo(selectedPiece.Minimum);
            if (WinCheck())
            {
                GetComponent<InputController>().acceptInput = false;
                winPanel.SetActive(true);
            }
        }
    }

    bool WinCheck()
    {
        Vector3 max = blocks[0].Maximum;
        Vector3 min = blocks[0].Minimum;
        foreach (var block in blocks)
        {
            Vector3 bmax = block.Maximum;
            Vector3 bmin = block.Minimum;
            if (bmax.x > max.x)
            {
                max.x = bmax.x;
            }
            if (bmin.x < min.x)
            {
                min.x = bmin.x;
            }
            if (bmax.y > max.y)
            {
                max.y = bmax.y;
            }
            if (bmin.y < min.y)
            {
                min.y = bmin.y;
            }
            if (bmax.z > max.z)
            {
                max.z = bmax.z;
            }
            if (bmin.z < min.z)
            {
                min.z = bmin.z;
            }
        }
        Vector3 bounds = max - min;
        if (bounds.x > x_length - 1 || bounds.y > y_height - 1 || bounds.z > z_width - 1)
        {
            return false;
        }
        return true;
    }
}