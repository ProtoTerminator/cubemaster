﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowLookAt : MonoBehaviour
{
    public Transform target;
    void Update()
    {
        Vector3 localPos = transform.InverseTransformDirection(target.position - transform.position);
        localPos.y = 0;
        Vector3 lookPos = transform.position + transform.TransformDirection(localPos);
        transform.LookAt(lookPos, transform.up);
    }
}