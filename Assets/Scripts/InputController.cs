﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public Camera cam;
    public Transform CameraCenter;
    Vector2 pointer;
    public CubeManager CM;
    Plane distPlane;
    Plane dragPlane;
    public bool freeformCamera = false;
    public bool invertedX = false;
    public bool invertedY = false;
    bool dragging = false;
    bool movingCamera = false;
    bool zooming = false;
    bool clickedArrow = false;
    public bool acceptInput = false;
    public bool focusTarget = false;
    int dragID;
    Vector3 startPos;
    Quaternion startRot;
    [SerializeField] float vertAngle = 30.178f;
    Vector3 cameraLookAt;
    bool cameraShifting = false;

    public Vector3 CameraLookAt
    {
        set
        {
            if (focusTarget && cameraLookAt != value)
            {
                StartCoroutine(CameraShift(value));
            }
            cameraLookAt = value;
        }
    }

    IEnumerator CameraShift(Vector3 target)
    {
        /*Quaternion start = cam.transform.rotation;
        Quaternion end;

        // calculate flat angle
        Vector3 flatLookStart = Vector3.Project(cam.transform.position - cameraLookAt, cam.transform.up) + cameraLookAt;
        float flatAngle = Vector3.Angle(flatLookStart - cameraLookAt, cameraLookAt - cameraLookAt);
        if (cameraLookAt.y > flatLookStart.y)
        {
            flatAngle = -flatAngle;
        }
        
        if (freeformCamera)
        {
            end = Quaternion.LookRotation(cameraLookAt - cam.transform.position, cam.transform.up);
        }
        else
        {
            end = Quaternion.LookRotation(cameraLookAt - cam.transform.position, cam.transform.up);
        }
        Debug.Log("start: " + start + ", end: " + end);
        float dist = Vector3.Distance(target, cam.transform.position);
        bool move = false;
        Vector3 startPos = cam.transform.position;
        Vector3 moveTo = cam.transform.position - target;
        if (dist > CM.Size * 5)
        {
            move = true;
            moveTo = (moveTo / dist) * 5 * CM.Size;
        }
        else if (dist < 5)
        {
            move = true;
            moveTo = (moveTo / dist) * 5;
        }
        moveTo = moveTo + target;*/

        Vector3 startVec = CameraCenter.transform.position;
        cameraShifting = true;
        float time = 0;
        while (time <= 0.5f)
        {
            yield return null;
            time += Time.deltaTime;
            /*cam.transform.rotation = Quaternion.Slerp(start, end, time * 2f);
            if (move)
            {
                cam.transform.position = Vector3.Slerp(startPos, moveTo, time * 2f);
                dist = Vector3.Distance(cam.transform.position, target);
                float scale = dist / 5;
                scale = scale / 4 + 0.75f;
                foreach (var arrow in CM.arrows)
                {
                    arrow.transform.localScale = Vector3.one * scale;
                }
            }*/
            CameraCenter.transform.position = Vector3.Lerp(startVec, target, time * 2f);
        }
        if (time > 0.5f)
        {
            CameraCenter.transform.position = target;
            //cam.transform.rotation = end;
            //cam.transform.position = moveTo;
        }
        /*vertAngle += flatAngle;
        Debug.Log("flatAngle: " + flatAngle + "\ntrueAngle: " + Quaternion.Angle(start, end));*/
        cameraShifting = false;
    }

    public void ToggleInvertX()
    {
        invertedX = !invertedX;
    }

    public void ToggleInvertY()
    {
        invertedY = !invertedY;
    }

    public void ToggleFreeform()
    {
        ResetCamera();
        freeformCamera = !freeformCamera;
    }

    public void ResetCamera()
    {
        cam.transform.position = startPos;
        cam.transform.rotation = startRot;
        vertAngle = 30.178f;
    }

    public void Begin()
    {
        cameraLookAt = CameraCenter.position;
        startPos = cam.transform.position;
        startRot = cam.transform.rotation;
        acceptInput = true;
    }

    void Update()
    {
        /*if (CM.SelectedPiece != null)
        {
            cameraLookAt = CM.SelectedPiece.clone.Center;
        }
        /*if (!cameraShifting)
        {
            if (freeformCamera)
            {
                //cam.transform.LookAt(cameraLookAt - cam.transform.position, cam.transform.up);
                cam.transform.rotation = Quaternion.LookRotation(cameraLookAt - cam.transform.position, cam.transform.up);
                //cam.transform.rotation = Quaternion.Lerp(cam.transform.rotation, Quaternion.LookRotation(cameraLookAt - cam.transform.position, cam.transform.up), Time.deltaTime * 2f);
            }
            else
            {
                //cam.transform.LookAt(cameraLookAt - cam.transform.position, (invertedX) ? Vector3.down : Vector3.up);
                cam.transform.rotation = Quaternion.LookRotation(cameraLookAt - cam.transform.position, (invertedX) ? Vector3.down : Vector3.up);
                //cam.transform.rotation = Quaternion.Lerp(cam.transform.rotation, Quaternion.LookRotation(cameraLookAt - cam.transform.position, (invertedX) ? Vector3.down : Vector3.up), Time.deltaTime * 2f);
            }
        }*/
        if (Input.touchSupported)
        {
            if (Input.touchCount == 0 || !acceptInput)
            {
                return;
            }
            Touch[] touches = Input.touches;
            if (Input.touchCount == 1 || movingCamera || dragging)
            {
                Touch touch = touches[0];
                if (Input.touchCount != 1)
                {
                    foreach (Touch tch in touches)
                    {
                        if (tch.fingerId == dragID)
                        {
                            touch = tch;
                            break;
                        }
                    }
                }
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        {
                            Ray ray = cam.ScreenPointToRay(touch.position);
                            RaycastHit hit;
                            if (Physics.Raycast(ray, out hit, 1 << 8))
                            {
                                if (hit.transform.tag == "Arrow") // start to drag arrow
                                {
                                    distPlane = new Plane(hit.transform.up, hit.point);
                                    dragPlane = new Plane(hit.transform.forward, hit.point);
                                    clickedArrow = true;
                                }
                            }
                        }
                        break;
                    case TouchPhase.Ended:
                        {
                            CM.CancelDrag();
                            if (!dragging && !movingCamera && !zooming)
                            {
                                bool selected = false;
                                Ray ray = cam.ScreenPointToRay(touch.position);
                                RaycastHit hit;
                                if (Physics.Raycast(ray, out hit))
                                {
                                    if (hit.transform.tag == "Cube") // clicked to select piece
                                    {
                                        selected = true;
                                        CM.SelectedPiece = hit.transform.parent.GetComponent<CubePieceParent>();
                                    }
                                }
                                if (!selected)
                                {
                                    CM.SelectedPiece = null;
                                }
                            }
                            else if (dragging)
                            {
                                CameraLookAt = CM.SelectedPiece.Center;
                            }
                            dragging = false;
                            clickedArrow = false;
                            movingCamera = false;
                            zooming = false;
                        }
                        break;
                    case TouchPhase.Moved:
                        {
                            dragging = clickedArrow;
                            movingCamera = !clickedArrow;
                            dragID = touch.fingerId;

                            if (movingCamera) // move camera
                            {
                                if (freeformCamera)
                                {
                                    Vector2 deltaPos = new Vector2(touch.deltaPosition.x, -touch.deltaPosition.y);
                                    if (invertedX)
                                    {
                                        deltaPos.x = -deltaPos.x;
                                    }
                                    if (invertedY)
                                    {
                                        deltaPos.y = -deltaPos.y;
                                    }
                                    Vector3 axis = cam.transform.up * deltaPos.x + cam.transform.right * deltaPos.y;
                                    cam.transform.RotateAround(CameraCenter.position, axis, Mathf.Abs(touch.deltaPosition.magnitude));
                                }
                                else
                                {
                                    cam.transform.RotateAround(CameraCenter.position, Vector3.up, (invertedX) ? -touch.deltaPosition.x : touch.deltaPosition.x);

                                    bool flipped = vertAngle > 90;
                                    float deltaAngle = (invertedY) ? touch.deltaPosition.y : -touch.deltaPosition.y;
                                    vertAngle += deltaAngle;
                                    cam.transform.RotateAround(CameraCenter.position, cam.transform.right, deltaAngle);
                                    if (!flipped)
                                    {
                                        if (deltaAngle > 0)
                                        {
                                            if (vertAngle > 90)
                                            {
                                                invertedX = !invertedX;
                                            }
                                        }
                                        else
                                        {
                                            if (vertAngle <= -90)
                                            {
                                                invertedX = !invertedX;
                                                vertAngle += 360;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (deltaAngle > 0)
                                        {
                                            if (vertAngle > 270)
                                            {
                                                invertedX = !invertedX;
                                                vertAngle -= 360;
                                            }
                                        }
                                        else
                                        {
                                            if (vertAngle <= 90)
                                            {
                                                invertedX = !invertedX;
                                            }
                                        }
                                    }
                                }
                            }
                            else // move cube
                            {
                                Ray ray = cam.ScreenPointToRay(touch.position);
                                float dist;
                                if (dragPlane.Raycast(ray, out dist))
                                {
                                    CM.TranslateSelectedPiece(distPlane.normal * distPlane.GetDistanceToPoint(ray.GetPoint(dist)));
                                    distPlane.SetNormalAndPosition(distPlane.normal, ray.GetPoint(dist));
                                }
                            }
                        }
                        break;
                    case TouchPhase.Stationary:
                        {
                            //movingCamera = false;
                        }
                        break;
                    default:
                        {
                            Debug.LogError("bad input");
                        }
                        break;
                }
            }
            else if (Input.touchCount == 2)
            {
                zooming = true;
                Touch touch0 = touches[0];
                Touch touch1 = touches[1];
                Vector2 prevTouch0 = touch0.position - touch0.deltaPosition;
                Vector2 prevTouch1 = touch1.position - touch1.deltaPosition;
                float prevTouchDelta = (prevTouch0 - prevTouch1).magnitude;
                float curTouchDelta = (touch0.position - touch1.position).magnitude;
                float deltaDiff = curTouchDelta - prevTouchDelta;

                float dist = Vector3.Distance(cam.transform.position, CameraCenter.position);
                if (deltaDiff > 0 && dist > 5 || deltaDiff < 0 && dist < 5 * CM.Size * 1.25f) // zoom camera
                {
                    cam.transform.position = Vector3.MoveTowards(cam.transform.position, CameraCenter.position, deltaDiff / 20);
                    dist = Vector3.Distance(cam.transform.position, CameraCenter.position);
                    float scale = dist / 5;
                    scale = scale / 4 + 0.75f;
                    foreach (var arrow in CM.arrows)
                    {
                        arrow.transform.localScale = Vector3.one * scale;
                    }
                }
            }
        }
        // mouse input
        else
        {
            if (Input.GetMouseButtonDown(0)) // click began
            {
                pointer = Input.mousePosition;
                Ray ray = cam.ScreenPointToRay(pointer);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 1 << 8))
                {
                    if (hit.transform.tag == "Arrow") // start to drag arrow
                    {
                        distPlane = new Plane(hit.transform.up, hit.point);
                        dragPlane = new Plane(hit.transform.forward, hit.point);
                        clickedArrow = true;
                    }
                }
            }
            else if (Input.GetMouseButtonUp(0)) // click ended
            {
                pointer = Input.mousePosition;
                CM.CancelDrag();
                if (!dragging && !movingCamera)
                {
                    bool selected = false;
                    Ray ray = cam.ScreenPointToRay(pointer);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit))
                    {
                        if (hit.transform.tag == "Cube") // clicked to select piece
                        {
                            selected = true;
                            CM.SelectedPiece = hit.transform.parent.GetComponent<CubePieceParent>();
                        }
                    }
                    if (!selected)
                    {
                        CM.SelectedPiece = null;
                    }
                }
                else if (dragging)
                {
                    CameraLookAt = CM.SelectedPiece.Center;
                }
                dragging = false;
                clickedArrow = false;
                movingCamera = false;
            }
            else if (Input.GetMouseButton(0))
            {
                if (pointer != (Vector2)Input.mousePosition) // moved mouse
                {
                    Vector2 deltaPos = (Vector2)Input.mousePosition - pointer;
                    dragging = clickedArrow;
                    movingCamera = !clickedArrow;

                    if (movingCamera) // move camera
                    {
                        if (freeformCamera)
                        {
                            deltaPos.y = -deltaPos.y;
                            if (invertedX)
                            {
                                deltaPos.x = -deltaPos.x;
                            }
                            if (invertedY)
                            {
                                deltaPos.y = -deltaPos.y;
                            }
                            Vector3 axis = cam.transform.up * deltaPos.x + cam.transform.right * deltaPos.y;
                            cam.transform.RotateAround(CameraCenter.position, axis, Mathf.Abs(deltaPos.magnitude));
                        }
                        else
                        {
                            cam.transform.RotateAround(CameraCenter.position, Vector3.up, (invertedX) ? -deltaPos.x : deltaPos.x);

                            bool flipped = vertAngle > 90;
                            float deltaAngle = (invertedY) ? deltaPos.y : -deltaPos.y;
                            vertAngle += deltaAngle;
                            cam.transform.RotateAround(CameraCenter.position, cam.transform.right, deltaAngle);
                            if (!flipped)
                            {
                                if (deltaAngle > 0)
                                {
                                    if (vertAngle > 90)
                                    {
                                        invertedX = !invertedX;
                                    }
                                }
                                else
                                {
                                    if (vertAngle <= -90)
                                    {
                                        invertedX = !invertedX;
                                        vertAngle += 360;
                                    }
                                }
                            }
                            else
                            {
                                if (deltaAngle > 0)
                                {
                                    if (vertAngle > 270)
                                    {
                                        invertedX = !invertedX;
                                        vertAngle -= 360;
                                    }
                                }
                                else
                                {
                                    if (vertAngle <= 90)
                                    {
                                        invertedX = !invertedX;
                                    }
                                }
                            }
                        }
                    }
                    else // move cube
                    {
                        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                        float dist;
                        if (dragPlane.Raycast(ray, out dist))
                        {
                            CM.TranslateSelectedPiece(distPlane.normal * distPlane.GetDistanceToPoint(ray.GetPoint(dist)));
                            distPlane.SetNormalAndPosition(distPlane.normal, ray.GetPoint(dist));
                        }
                    }
                }
                else // stationary mouse
                {
                    //movingCamera = false;
                }
                pointer = Input.mousePosition;
            }
        }
    }
}