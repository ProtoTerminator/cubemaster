﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrientationWatcher : MonoBehaviour
{
    public Camera cam;
    float defaultField = 60;

    private void Start()
    {
        OnRectTransformDimensionsChange();
    }

    private void OnRectTransformDimensionsChange()
    {
        if (Screen.width > Screen.height)
        {
            cam.fieldOfView = defaultField;
        }
        else
        {
            float magicToPortrait = Mathf.Sqrt((float) Screen.height / Screen.width) * Mathf.Tan(defaultField * Mathf.Deg2Rad * 0.5F);
            cam.fieldOfView = Mathf.Atan(magicToPortrait / Mathf.Sqrt((float)Screen.width / Screen.height)) * Mathf.Rad2Deg * 2;
            //cam.fieldOfView = defaultField * Screen.height / Screen.width;
        }
    }
}