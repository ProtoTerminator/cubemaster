﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubePieceParent : MonoBehaviour
{
    public CubePieceParent clone;
    public List<CubePiece> children = new List<CubePiece>();
    [SerializeField]
    Vector3 max;
    [SerializeField]
    Vector3 min;
    public Vector3 Maximum
    {
        get
        {
            return transform.position + max;
        }
    }
    public Vector3 Minimum
    {
        get
        {
            return transform.position + min;
        }
    }
    public Vector3 Center
    {
        get
        {
            return (Minimum + Maximum) / 2;
        }
    }

    public void MoveMinimumTo(Vector3 target)
    {
        transform.Translate(target - Minimum);
        foreach (var cube in children)
        {
            cube.X = (int)cube.transform.position.x;
            cube.Y = (int)cube.transform.position.y;
            cube.Z = (int)cube.transform.position.z;
        }
    }

    public void InitBounds()
    {
        max = children[0].transform.position;
        min = max;
        foreach (var child in children)
        {
            Vector3 pos = child.transform.position;
            if (pos.x > max.x)
            {
                max.x = pos.x;
            }
            if (pos.y > max.y)
            {
                max.y = pos.y;
            }
            if (pos.z > max.z)
            {
                max.z = pos.z;
            }
            if (pos.x < min.x)
            {
                min.x = pos.x;
            }
            if (pos.y < min.y)
            {
                min.y = pos.y;
            }
            if (pos.z < min.z)
            {
                min.z = pos.z;
            }
        }
    }
}