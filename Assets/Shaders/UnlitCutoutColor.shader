﻿Shader "Unlit/Texture Color Cutout"
 {
     Properties
     {
         _Color ("Color Tint", Color) = (1,1,1,1)   
		 _MainTex("Base (RGB) Alpha (A)", 2D) = "white" {}
		_Cutoff("Alpha cutoff", Range(0,1)) = 0.5
     }
 
     Category
     {
         Lighting Off
		 AlphaTest Greater [_Cutoff]
 
         SubShader
         {
              Pass
              {
                    SetTexture [_MainTex]
                    {
						ConstantColor [_Color]
						Combine Texture * constant
                 }
             }
         }
     }
 }